$(document).ready(function () {

    $('#toggle-nav').click(function () {
        // console.log(this);
        var sidebar = $('.sidebar');

        if (sidebar.hasClass('open-nav')) {
            sidebar.removeClass('open-nav');
            $(this).html('<i class="fa fa-chevron-circle-left"><i/>');
            $('.container-body').css('margin', '0 auto');
        } else {
            sidebar.addClass('open-nav');
            $(this).html('<i class="fa fa-chevron-circle-right"><i/>');
            $('.container-body').css('margin', '0');
        }
    });

    $('body').on('click', '[rel="popover"]', function (e) {
        $('.popover').remove();
    });

    $('#edit-profile').click(function () {
        $('input:text').removeAttr('disabled');
        $('textarea').removeAttr('disabled');
        $(this).css('display', 'none');
        $('#save-profile').css('display', 'block');
    });

    $('#edit-password').click(function () {
        $('input:password').removeAttr('disabled');
        $('input:password').addClass('edit');
    });

    //Product List PopUp
    $('#kecamatan').popover({
        html: true,
        content: function () {
            return $("#content-kecamatan").html();
        }
    });

    $('#sorting').popover({
        html: true,
        content: function () {
            return $("#content-sorting").html();
        }
    });

    $('#kategori').popover({
        html: true,
        content: function () {
            return $("#content-kategori").html();
        }
    });
});